
import boto3
import os
import json


def lambda_handler(event, context):
    print("iniciando evento")
    print(event)
    data = event["message"]
    print(data)
    client = boto3.client('iot-data')
    print(type(data))
    if data >= 1.5:
        response = client.publish(
            topic='test/action',
            qos=0,
            payload=json.dumps({"message":"mala postura"})
        )
        print(response)
    print("evento Terminado")
    
    
    return event["message"]
    
